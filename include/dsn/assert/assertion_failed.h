// -*- C++ -*-
#pragma once

#include <exception>
#include <sstream>
#include <string>

#include <dsn/assert/exports.h>

namespace dsn {
namespace assert {
    /**
     * @brief Exception for failed assertions
     */
    class DSN_ASSERT_EXPORT assertion_failed : public std::exception {
    public:
        assertion_failed(const char* expression, const char* file, int line, const std::string& message);

        virtual ~assertion_failed() = default;

        /**
         * @brief Helper class for stream-style message formatting
         */
        class DSN_ASSERT_EXPORT formatter {
        private:
            std::stringstream m_stream; //!< Buffered message text

        public:
            operator std::string();

            /**
             * @brief Append value to message buffer
             *
             * @param value Value that shall be added to the message buffer
             *
             * @tparam T Type of `value`
             */
            template <typename T> formatter& operator<<(const T& value)
            {
                m_stream << value;
                return *this;
            }
        };

        const char*        expression() const;
        const char*        filename() const;
        int                line() const;
        const std::string& message() const;

        // exception interface
    public:
        virtual const char* what() const noexcept override;

    private:
        const char*         m_expression; //!< Failed expression
        const char*         m_filename;   //!< Filename that triggered the exception
        int                 m_line;       //!< Line number that triggered the exception
        std::string         m_message;    //!< Caller-supplied error message
        mutable std::string m_what;       //!< Complete error message for what()
    };
}
}
