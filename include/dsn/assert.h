// -*- C++ -*-
#pragma once

#include <exception>
#include <iostream>

#include "assert/assertion_failed.h"

#ifndef NDEBUG

// regular assert macro that calls std::terminate
#define dsn_assert(EXPRESSION, MESSAGE)                                                                                \
    {                                                                                                                  \
        if (!(EXPRESSION)) {                                                                                           \
            std::cerr << "Assertion '" << (#EXPRESSION) << "' failed in " << __FILE__ << ":" << __LINE__ << ": "       \
                      << MESSAGE << std::endl;                                                                         \
            std::terminate();                                                                                          \
        }                                                                                                              \
    }

// assert macro that throws an exception
#define dsn_throwing_assert(EXPRESSION, MESSAGE)                                                                       \
    {                                                                                                                  \
        if (!(EXPRESSION)) {                                                                                           \
            throw dsn::assert::assertion_failed((#EXPRESSION), __FILE__, __LINE__,                                     \
                                                (dsn::assert::assertion_failed::formatter() << MESSAGE));              \
        }                                                                                                              \
    }

#else

#define dsn_assert(EXPRESSION, MESSAGE)

#define dsn_throwing_assert(EXPRESSION, MESSAGE)

#endif // NDEBUG
