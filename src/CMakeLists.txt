include(GenerateExportHeader)

add_library(dsn_assert SHARED
    ../include/dsn/assert.h
    ../include/dsn/assert/assertion_failed.h assertion_failed.cpp)

set_target_properties(dsn_assert PROPERTIES
    VERSION ${dsn_assert_VERSION_MAJOR}.${dsn_assert_VERSION_MINOR}.${dsn_assert_VERSION_PATCH}
    SOVERSION ${dsn_assert_VERSION_MAJOR}.${dsn_assert_VERSION_MINOR})

generate_export_header(dsn_assert
    EXPORT_FILE_NAME ${dsn_assert_BUILD_ROOT}/include/dsn/assert/exports.h)
install(FILES ${dsn_assert_BUILD_ROOT}/include/dsn/assert/exports.h DESTINATION include/dsn/assert)
target_sources(dsn_assert PRIVATE ${dsn_assert_BUILD_ROOT}/include/dsn/assert/exports.h)

target_include_directories(dsn_assert PUBLIC
    $<BUILD_INTERFACE:${dsn_assert_SOURCE_ROOT}/include>
    $<INSTALL_INTERFACE:include/>)
target_include_directories(dsn_assert PUBLIC
    $<BUILD_INTERFACE:${dsn_assert_BUILD_ROOT}/include>
    $<INSTALL_INTERFACE:include/>)

if(dsn_assert_WITH_COMPILE_FEATURES)
    target_compile_features(dsn_assert PUBLIC cxx_deleted_functions)
endif(dsn_assert_WITH_COMPILE_FEATURES)

install(TARGETS dsn_assert EXPORT dsn_assert-targets
    RUNTIME DESTINATION bin ARCHIVE DESTINATION lib LIBRARY DESTINATION lib)
