#include <dsn/assert/assertion_failed.h>

/**
 * @brief Create new exception instance
 * @param expression Assertion that triggered the exception
 * @param file Source file where the exception was triggered
 * @param line Line number in `file`
 * @param message Caller-supplied error message
 */
dsn::assert::assertion_failed::assertion_failed(const char* expression, const char* file, int line,
                                                const std::string& message)
    : m_expression(expression)
    , m_filename(file)
    , m_line(line)
    , m_message(message)
{
}

/**
 * @brief Accessor for failed expression
 * @return String containing the expression that failed
 */
const char* dsn::assert::assertion_failed::expression() const { return m_expression; }

/**
 * @brief Accessor for source file
 * @return String containing the source file that triggered the exception
 *
 * @see line
 */
const char* dsn::assert::assertion_failed::filename() const { return m_filename; }

/**
 * @brief Accessor for line number
 * @return Line number where the exception was triggered
 *
 * @see filename
 */
int dsn::assert::assertion_failed::line() const { return m_line; }

/**
 * @brief Accessor for caller-supplied error message
 * @return String containing the error message that was supplied in the assertion
 */
const std::string& dsn::assert::assertion_failed::message() const { return m_message; }

/**
 * @brief Get cause of exception
 * @return String containing a description of the failed assertion as well as its source location
 */
const char* dsn::assert::assertion_failed::what() const noexcept
{
    if (m_what.empty()) {
        std::stringstream msg;
        msg << "Assertion '" << m_expression << "' failed in " << m_filename << ":" << m_line << ": " << m_message;
        m_what = msg.str();
    }

    return m_what.c_str();
}

/**
 * @brief Convert to std::string
 */
dsn::assert::assertion_failed::formatter::operator std::string() { return m_stream.str(); }
