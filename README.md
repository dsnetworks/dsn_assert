# libdsn_assert README
This library provides a customized `assert(...)` macro with support for stream-style message formatting and exceptions.

## Dependencies
* C++11 or newer compiler with support for deleted functions
* [Boost](https://www.boost.org/) for unit tests

## Building
Use CMake to configure/build.

### Build options
* `dsn_assert_WITH_TESTS` - Compile unit tests
* `dsn_assert_WITH_CPACK` - Include CPack to generate binary packages
