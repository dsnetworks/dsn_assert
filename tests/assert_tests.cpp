#define BOOST_TEST_MODULE

#ifdef NDEBUG
#undef NDEBUG
#endif // NDEBUG

#include <dsn/assert.h>

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE(assertion_pass) { dsn_assert(1 > 0, "One is smaller than zero?!"); }

BOOST_AUTO_TEST_CASE(throwing_assertion_pass)
{
    BOOST_REQUIRE_NO_THROW(dsn_throwing_assert(1 > 0, "One is smaller than zero?!"));
}

BOOST_AUTO_TEST_CASE(throwing_assertion_fail)
{
    BOOST_REQUIRE_THROW(dsn_throwing_assert(0 > 1, "Expected assertion failure"), dsn::assert::assertion_failed);
}
