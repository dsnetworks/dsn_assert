find_package(Boost REQUIRED COMPONENTS unit_test_framework)

add_executable(assert_tests assert_tests.cpp ../include/dsn/assert.h)
target_link_libraries(assert_tests dsn_assert)
target_link_libraries(assert_tests ${Boost_LIBRARIES})
target_include_directories(assert_tests PRIVATE ${Boost_INCLUDE_DIRS})
target_compile_definitions(assert_tests PRIVATE -DBOOST_TEST_DYN_LINK)

add_test(assert_tests assert_tests)
